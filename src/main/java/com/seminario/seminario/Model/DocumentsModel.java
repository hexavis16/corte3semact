package com.seminario.seminario.Model;

import com.seminario.seminario.Entity.Documents;

import java.util.Date;

public class DocumentsModel {

    public DocumentsModel(long id, String docnum, String picture, String name, String description, String city, String country,
                     Date datedoc, boolean status, String contactnumber, String place, String type) {

            this.id = id;
            this.docnum = docnum;
            this.picture = picture;
            this.name = name;
            this.description = description;
            this.city = city;
            this.country = country;
            this.datedoc = datedoc;
            this.status = status;
            this.contactnumber = contactnumber;
            this.place = place;
            this.type = type;
    }

    public DocumentsModel(Documents documents) {

        this.id = documents.getId();
        this.docnum = documents.getDocnum();
        this.picture = documents.getPicture();
        this.name = documents.getName();
        this.description = documents.getDescription();
        this.city = documents.getCity();
        this.country = documents.getCountry();
        this.datedoc = documents.getDatedoc();
        this.status = documents.isStatus();
        this.contactnumber = documents.getContactnumber();
        this.place = documents.getPlace();
        this.type = documents.getType();
    }

    public DocumentsModel() {}

    private long id;
    private String docnum;
    private String picture;
    private String name;
    private String description;
    private String city;
    private String country;
    private Date datedoc;
    private boolean status;
    private String contactnumber;
    private String place;
    private String type;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDocnum() {
        return docnum;
    }

    public void setDocnum(String docnum) {
        this.docnum = docnum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Date getDatedoc() {
        return datedoc;
    }

    public void setDatedoc(Date datedoc) {
        this.datedoc = datedoc;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getContactnumber() {
        return contactnumber;
    }

    public void setContactnumber(String contactnumber) {
        this.contactnumber = contactnumber;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
