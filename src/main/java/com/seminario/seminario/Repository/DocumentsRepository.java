package com.seminario.seminario.Repository;

import com.seminario.seminario.Model.DocumentsModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DocumentsRepository extends JpaRepository <DocumentsModel, Long>{

}
